# CI Interpolation Example

This project shows how you can use CI Interpolation, a new feature, recently
introduced in 15.11.

The CI Interpolation allows you to customize any file, that can be included
using `include` keyword in `.gitlab-ci.yml`.

In order to enable it, you need to define a template file with a leading
specification YAML document, that will describe what inputs arguments may be
used with the template:

```yaml
# my-template.yml

spec:
  inputs:
    website:
    stage:
      default: deploy
---
deploy:
  stage: $[[ inputs.stage ]]
  script: echo "deploy $[[ inputs.website ]]"
```

Then you can include the file using various techniques that the [`include:`
keyword](https://docs.gitlab.com/ee/ci/yaml/#include) supports:


```yaml
# .gitlab-ci.yml

include:
  - local: my-template.yml
    inputs:
      stage: deploy
      website: my-website.example.com

stages:
 - test
 - deploy

test:
  stage: test
  script: echo "test"
```

Run the first batch.
